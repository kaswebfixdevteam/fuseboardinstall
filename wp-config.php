<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */


// * MySQL settings - You can get this info from your web host ** //
if($_SERVER['HTTP_HOST']=="localhost" || $_SERVER['SERVER_NAME']=="localhost"){
/*
WHEN USING ANY KIND OF LOCALHOST, IT WILL USE THESE DATABASE CONNECTION DETAILS
*/
  if($_SERVER['HTTP_HOST']=="localhost:8888" || $_SERVER['SERVER_PORT']=="8888"){
	/*
	THESE ARE CONNECTION DETAILS FOR A MAC
	
	die("dad 1");*/
	  define('DB_NAME', 'righttrades_services');
	  define('DB_USER', 'mampuser');
	  define('DB_PASSWORD', 'i43rDSrWef£3$R!5');
	  define('DB_HOST', 'localhost');
  }else{
	/*
	THESE ARE CONNECTION DETAILS FOR A PC (LIKE YOURS, BPE)
	
	//die("dad 2");*/
	  define('DB_NAME', 'fuseboardinstall');
	  define('DB_USER', 'root');
	  define('DB_PASSWORD', '');
	  define('DB_HOST', 'localhost');
  }
}else{
	/*
	THESE ARE THE >>LIVE<< CONNECTION DETAILS FOR THE WORLD TO SEE
	
	die("dad 3");*/
  define('DB_NAME', 'righttrades_services');
  define('DB_USER', 'root');
  define('DB_PASSWORD', 'xxxxxxxxxxx');
  define('DB_HOST', 'localhost');
}
/*
Site Title:   Fuse Board Install
Usernmae:     fuseboardinstall
Password:     z!E$vq9OLP7056oQ$Rz!E$vq9OLP70
Your email:   wp@fuseboardinstall.com

ah ok we are here
*/




/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'put your unique phrase here');
define('SECURE_AUTH_KEY',  'put your unique phrase here');
define('LOGGED_IN_KEY',    'put your unique phrase here');
define('NONCE_KEY',        'put your unique phrase here');
define('AUTH_SALT',        'put your unique phrase here');
define('SECURE_AUTH_SALT', 'put your unique phrase here');
define('LOGGED_IN_SALT',   'put your unique phrase here');
define('NONCE_SALT',       'put your unique phrase here');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
